<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'e@D64*YH0N-~RmQ_D,[]qEumpN|,I<.sQQ=AQU!N<i9[NCpWCh_DBO`zr13&!W]X' );
define( 'SECURE_AUTH_KEY',  'eZJMe8N)zsz2Esn9J^}U<wgFDiUN4BYA|MJ}%^M}y[!(gUEQUn+(;atI O_T22lM' );
define( 'LOGGED_IN_KEY',    'z_RcJ;J$Cn?%M2c)S4`*W;k1<mWS6!3!&)oW=tR* aEnr?qw>e0TK*stvXrT?WEu' );
define( 'NONCE_KEY',        ';7xwCL;6A *X$1ulP)rD&S6s7d2;(>FtxRf,SK]eyKTr(jV#F`F5z5W5,M3UEh>x' );
define( 'AUTH_SALT',        'OqnR>42~cTqYwCvCO-IuBsf``Z*1eV>$=NPM&`{3*)alBa1=f  5J3G?BwjnEu/~' );
define( 'SECURE_AUTH_SALT', 'HUlH=Ai0>)=f+qLE8}^QJ;@}z]c)^P0_K)#n?$KtREbGTO?=D%mn)^xQjI>YHR)p' );
define( 'LOGGED_IN_SALT',   '?zGTON#R=mjMt}$wQ d{8z)l&+{Nr;(mP;IF4vp{yb8PRu>D4Oz:WI9wO3#?/_!v' );
define( 'NONCE_SALT',       'ufI_6u2L,f3wr2o0l)!`va^Hk%N!=oE_t7_2UoU,:T%~,au,&hWOc{Xe-KJ^hOB|' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
ini_set('log_errors','On');

ini_set('display_errors','Off');

ini_set('error_reporting', E_ALL );

define('WP_DEBUG', false);

define('WP_DEBUG_LOG', true);

define('WP_DEBUG_DISPLAY', false);
/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
