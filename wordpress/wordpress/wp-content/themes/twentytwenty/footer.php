<?php
/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?>
	<footer>
      <div class="container">
        <div class="footer-main">
          <p>The information contained on this website represents the views and opinions of the individuals who have completed the reviews and do not reflect the views and opinions of anyone associated with MySuperRating. The primary purpose of this website is to provide a platform for superannuation members to rate their superannuation fund and to review the ratings provided to other superannuation funds. It is the responsibility of the individual to conduct their own detailed research on the features, fees and costs and returns of the individual funds and not rely solely on the ratings displayed on this website</p>
        </div>
      </div>
      <div class="copy-right">
        Copyright 2020  | All Rights Reserved by MySuperRating
      </div>
    </footer>

			<!-- #site-footer -->

		<?php wp_footer(); ?>

		<!-- <script src="https://code.jquery.com/jquery-3.5.1.min.js"
    integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script> -->
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
    crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
    integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
    crossorigin="anonymous"></script>


	</body>
</html>
